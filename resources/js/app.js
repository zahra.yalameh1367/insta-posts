/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

require('./theme/overlayscrollbars/OverlayScrollbars.min');
// <script src="https://polyfill.io/v3/polyfill.min.js?features=window.scroll"></script>


 require('./theme/popper/popper.min.js');
 require('./theme/anchorjs/anchor.min.js');
 require('./theme/is/is.min.js');
 require('./theme/swiper/swiper-bundle.min.js');
 require('./theme/lodash/lodash.min.js');
 require('./theme/list.js/list.min.js');
 require('./theme/theme.js');
 require('./components/App');
