import React from "react";
import { BrowserRouter as Router, Routes , Route } from "react-router-dom";
import MainPage from "../../Pages/Main";

const AppRoutes = () => (
    <Router>
        <Routes  >
            <Route exact path="/" element={<MainPage />}  ></Route>
            <Route path="/:category_id" element={<MainPage />}  ></Route>
        </Routes>
    </Router>
);

export default AppRoutes;