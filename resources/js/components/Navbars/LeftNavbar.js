import React , {Component} from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import * as Icons from '@fortawesome/free-solid-svg-icons';

class LeftNavbar extends Component {
  
  state = {
    categories : []
  }

  componentDidMount(){
    
      axios('/api/categories').then(response => {        
            this.setState({categories : [...response.data]})
        })
        .catch(error => {
            console.log(error)
      });
  }

  render(){

    return <>
    <nav className="navbar navbar-light navbar-vertical navbar-expand-xl navbar-vibrant">
        <div className="d-flex align-items-center">
            <div className="toggle-icon-wrapper">
              <button className="btn navbar-toggler-humburger-icon navbar-vertical-toggle" data-bs-toggle="tooltip" data-bs-placement="left" title="Toggle Navigation"><span className="navbar-toggle-icon"><span className="toggle-line"></span></span></button>
            </div>
            <Link to="/" className="navbar-brand" >
              <div className="d-flex align-items-center py-3"><img className="me-2" src="img/icons/logo.png" alt="" width="40" /><span className="font-sans-serif">Inista Posts</span>
              </div>
            </Link>
        </div>
          
        <div className="collapse navbar-collapse" id="navbarVerticalCollapse">
            <div className="navbar-vertical-content scrollbar">
              <ul className="navbar-nav flex-column mb-3" id="navbarVerticalNav">

                <li className="nav-item mt-3">
                    <Link to="/" className="nav-link" role="button" >
                      <div className="d-flex align-items-center"><span className="nav-link-icon"><FontAwesomeIcon icon={Icons['faChartPie']} /></span>
                          <span className="nav-link-text ps-1 font-weight-bold">Home</span>
                      </div>
                    </Link>                   
                </li>

                <li className="nav-item">
                  <div className="row navbar-vertical-label-wrapper mt-3 mb-2">
                    <div className="col-auto navbar-vertical-label">Categories
                    </div>
                    <div className="col ps-0">
                      <hr className="mb-0 navbar-vertical-divider" />
                    </div>
                  </div>
                  {this.state.categories.map(category =>(
                    
                    <Link key={category.id} to={`/${category.id}`} className="nav-link" role="button" aria-expanded="false">
                      <div className="d-flex align-items-center">
                        <span className="nav-link-icon">
                        <FontAwesomeIcon icon={Icons[category.icon]} />
                        </span>
                          <span className="nav-link-text ps-1">{category.title}</span>
                      </div>
                    </Link>
                  )
                  )
                  }
 
                </li>
                
              </ul>
             
            </div>
          </div>
        </nav>
      </>
  }
};

export default LeftNavbar;