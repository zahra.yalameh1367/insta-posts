import { Types } from "./actionTypes";

export const fetchPosts = ({_limit , _page,_category_id}) => {
    
  var url = `posts?limit=${_limit}&page=${_page}`;

  if(_category_id){
    url = url + `&category_id=${_category_id}`;
  }
    return {
        type : Types.FETCH_POSTS,
        successType : Types.FETCH_POSTS_SUCCESS,
        errorType : Types.FETCH_POSTS_FAILED,
        isEndPointCall : true,
        endpoint : url,
        method : 'get'
    };
};


export const deletePost = (id) => {
console.log('deletePost')
    
    return {
        type : Types.DELETE_POST,
        successType : Types.DELETE_POST_SUCCESS,
        errorType : Types.DELETE_POST_FAILED,
        isEndPointCall : true,
        endpoint : `posts/${id}`,
        method : 'delete',
        reduxData : {id}
    };
};

export const deletePostAndFetch = (id, filter) => {
console.log('deletePostAndFetch')

    return dispatch => {
      dispatch(deletePost(id)).then(response => {
        if (response.status === 200) {
          dispatch(fetchPosts(filter));
        }
      });
    };
  };
  