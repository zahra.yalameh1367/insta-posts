import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowLeft, faArrowRight } from '@fortawesome/free-solid-svg-icons'

const Pagination = ({ limit , page  , onPageChange, total}) => {

    const onPrevClick = () => onPageChange( page - 1);
    const onNextClick = () => onPageChange(page + 1);

  return (
    <nav aria-label="Page navigation example">
    <ul className="pagination">
        <li className="page-item" onClick={onPrevClick} ><button className={"page-link" + (page <= 1 ? ' disabled' : '')} ><FontAwesomeIcon icon={faArrowLeft} /></button></li>
        <li className="page-item" onClick={() => onPageChange(1)} ><button className="page-link" >1</button></li>
        <li className="page-item" onClick={() => onPageChange(2)} ><button className="page-link" >2</button></li>
        <li className="page-item" onClick={() => onPageChange(3)} ><button className="page-link" >3</button></li>
        <li className="page-item" onClick={onNextClick} ><button className={"page-link" + ( ((page+1)*limit) >= total ? ' disabled' : '')} ><FontAwesomeIcon icon={faArrowRight} /></button></li>
    </ul>
    </nav>
  );
};

export default Pagination;
