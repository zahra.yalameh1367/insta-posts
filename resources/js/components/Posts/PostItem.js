import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash } from '@fortawesome/free-solid-svg-icons'

const PostItem = ({ post , deletePost }) => {

    const onDeletePost = () => {

        if(window.confirm('Are you sure')){
            deletePost(post.id);
        }
    };
  return (
    <div className="mb-4 col-md-6 col-lg-4">
        <div className="border rounded-1 h-100 d-flex flex-column justify-content-between pb-3">
            <div className="overflow-hidden">
            <div className="position-relative rounded-top overflow-hidden">
                <a className="d-block" href="../../../app/e-commerce/product/product-details.html">
                    <img className="img-fluid rounded-top" src={"img/blog/" + post.image} alt={post.title} /></a>
                    <span className="badge rounded-pill bg-success position-absolute mt-2 me-2 z-index-2 top-0 end-0">New</span>
            </div>
            <div className="p-3">
            <h5 className="fs-0"><a className="text-dark" href="../../../app/e-commerce/product/product-details.html">{post.title}</a></h5>
            <p className="fs--1 mb-3"><a className="text-500" href="#!">Computer &amp; Accessories</a></p>
            <h5 className="fs-md-2 text-warning mb-0 d-flex align-items-center mb-3"> $1199.5
                <del className="ms-2 fs--1 text-500">$2399 </del>
            </h5>
            <p className="fs--1 mb-1">Shipping Cost: <strong>$50</strong></p>
            <p className="fs--1 mb-1">Stock: <strong className="text-success">Available</strong>
            </p>
            <p className="card-text">{post.body}</p>
            </div>
        </div>
        <div className="d-flex flex-between-center px-3">
            <div><span className="fa fa-star text-warning"></span><span className="fa fa-star text-warning"></span><span className="fa fa-star text-warning"></span><span className="fa fa-star text-warning"></span><span className="fa fa-star text-300"></span><span className="ms-1">(8)</span>
            </div>
            <div><a className="btn btn-sm btn-falcon-default me-2" href="#!" data-bs-toggle="tooltip" data-bs-placement="top" title="Add to Wish List"><span className="far fa-heart"></span></a><a className="btn btn-sm btn-falcon-default" href="#!" data-bs-toggle="tooltip" data-bs-placement="top" title="Add to Cart"><span className="fas fa-cart-plus"></span></a></div>
            <button className="btn" onClick={onDeletePost} ><FontAwesomeIcon icon={faTrash} /></button>
        </div>
        </div>
    </div>
  );
};

export default PostItem;
