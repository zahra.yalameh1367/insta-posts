import React from "react";
import PostItem from "./PostItem";

const PostList = ({ posts , deletePost }) => (
  <>
    {posts.map(post => (
      <PostItem key={post.id} post={post} deletePost={deletePost} />
    ))}
  </>
);

export default PostList;
