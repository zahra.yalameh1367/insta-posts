import React , {Component} from "react";
import { useParams } from "react-router-dom";
import PostConrainer from "../Posts/PostConrainer";
import LeftNavbar from "../Navbars/LeftNavbar";
import TopNavbar from "../Navbars/TopNavbar";
import TopSort from "./TopSort";
import PageFooter from "./PageFooter";

const MainPage = () => {

    const {category_id} = useParams();

    return <>
        <LeftNavbar />
        <div className="content">
            <TopNavbar />
            <TopSort />
            <div className="card mb-3">
                <div className="card-body"  >  
                <PostConrainer category_id={category_id} />          
                </div>
                
                <div className="card-footer bg-light d-flex justify-content-center">
                    <div>
                    <button className="btn btn-falcon-default btn-sm me-2" type="button" disabled="disabled" data-bs-toggle="tooltip" data-bs-placement="top" title="Prev"><span className="fas fa-chevron-left"></span></button><a className="btn btn-sm btn-falcon-default text-primary me-2" href="#!">1</a><a className="btn btn-sm btn-falcon-default me-2" href="#!">2</a><a className="btn btn-sm btn-falcon-default me-2" href="#!"> <span className="fas fa-ellipsis-h"></span></a><a className="btn btn-sm btn-falcon-default me-2" href="#!">35</a>
                    <button className="btn btn-falcon-default btn-sm" type="button" data-bs-toggle="tooltip" data-bs-placement="top" title="Next"><span className="fas fa-chevron-right">     </span></button>
                    </div>
                </div>          
            </div>
            
            <PageFooter />      
        </div>
        </>
    
    
}

export default MainPage;