import React from "react";

const TopSort = () => (
    <>
    <div className="card mb-3">
        <div className="card-body">
            <div className="row flex-between-center">
            <div className="col-sm-auto mb-2 mb-sm-0">
                <h6 className="mb-0">Showing 1-24 of 205 Products</h6>
            </div>
            <div className="col-sm-auto">
                <div className="row gx-2 align-items-center">
                <div className="col-auto">
                    <form className="row gx-2">
                    <div className="col-auto"><small>Sort by:</small></div>
                    <div className="col-auto">
                        <select className="form-select form-select-sm" aria-label="Bulk actions">
                        <option>Best Match</option>
                        <option value="Refund">Newest</option>
                        <option value="Delete">Price</option>
                        </select>
                    </div>
                    </form>
                </div>
                <div className="col-auto pe-0"> <a className="text-600 px-1" href="../../../app/e-commerce/product/product-list.html" data-bs-toggle="tooltip" data-bs-placement="top" title="Product List"><span className="fas fa-list-ul"></span></a></div>
                </div>
            </div>
            </div>
        </div>
    </div>
    </>
);

export default TopSort;