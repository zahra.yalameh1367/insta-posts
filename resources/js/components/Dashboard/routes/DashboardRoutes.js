import React from "react";
import { Route } from "react-router-dom";
import Dashboard from "../Dashboard";


const DashboardRoutes = () => (
    <Route exact path="/" element={<Dashboard />}  ></Route>
);

export default DashboardRoutes;