import React from "react";
import { Link } from "react-router-dom";

const Dashboard = () => (<div>
    <Link to="/posts" >Posts</Link>
    <h1>Dashboard</h1>
</div>
);

export default Dashboard;