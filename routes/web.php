<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/{path?}', 'layout');
Route::post('change_language', function() {
    $locale = Input::get('locale');
    App::setLocale($locale);
});

/*$(document).on('change', '#language_selector' , function() {
    var locale = $(this).val();
    $.ajax({
        url: "{{ url('change_language') }}",
        type: "POST",
        data: {
            locale: locale
        },
        success: function() {
            window.location.reload();
        }
    });
});
*/