<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'author',
        'body',
        'user_id'
    ];

    public function tableParams($params){    
					
		$params['page'] = (isset($params['page']))?(int)$params['page'] + 1:1;
		$params['limit'] = (isset($params['limit'])  && $params['limit']>=0 )?(int)$params['limit']:20;		
		
		return $params;
	}

    public function postList($params){
		
        global $dbpre;

    	$params = self::tableParams($params);

    	Paginator::currentPageResolver(function () use ($params) {
			return $params['page'];
		});

		DB::enableQueryLog();
		$table_result = Post::select(['*'])	
            ->where(function ($query) use ($params) {
                if(!empty($params['category_id'])) { $query->where('category_id','=',$params['category_id']); }
            })
            ->orderBy('id','desc')                       
            ->paginate($params['limit'])
            ->toArray();
				
			return $table_result;	
		
	}
}
