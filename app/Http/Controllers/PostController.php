<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;

class PostController extends Controller
{
    //

    public function getPostList(Request  $request)
    {
        
        try{
            
            $params = ['limit' => $request->input('limit'),
                    'page' => $request->input('page'),
                    'category_id' => (int)$request->input('category_id') ];
            
            $post_model = new Post();
            $posts = $post_model->postList($params);

            return response()->json($posts);
        }
        catch(Exception $e){
            Log::error($e);
        }
    }
    public function deletePost($id)
    {
        try{
                        
            $post_model = new Post();
            $posts = $post_model->where('id','=',$id)->delete();

            return response()->json(['status' => 'success']);
        }
        catch(Exception $e){
            Log::error($e);
        }
    }
}
