<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Categories;

class CategoryController extends Controller
{
    
    public function getCatergoryList()
    {
        
        try{
            
            $post_model = new Categories();
            $posts = $post_model->get()->toArray();

            return response()->json($posts);
        }
        catch(Exception $e){
            Log::error($e);
        }
    }
}
