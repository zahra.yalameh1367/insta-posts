const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 
*/
let WebpackRTLPlugin = require('webpack-rtl-plugin');

mix.js('resources/js/app.js', 'public/js')
.react()
  .sass('resources/sass/app.scss', 'public/css')
  .webpackConfig({
    plugins: [
      new WebpackRTLPlugin()
    ]
  });

mix.options({
    processCssUrls: false // Process/optimize relative stylesheet url()'s. Set to false, if you don't want them touched.
});